USE master
IF EXISTS (SELECT *FROM sys.databases WHERE name = 'TrainingManagement')
DROP DATABASE TrainingManagement
CREATE DATABASE TrainingManagement
GO

USE TrainingManagement
CREATE TABLE dbo.Employee
(
	EmpNo INT PRIMARY KEY,
	EmpName VARCHAR(225),
	BirthDay DATE ,
	DeptNo INT,
	MgrNo INT,
	StartDate DATE,
	Salary DECIMAL,
	Level INT,
	Status INT,
	Note VARCHAR (MAX)
)
GO

CREATE TABLE dbo.Department
(
	DeptNo INT IDENTITY PRIMARY KEY,
	DeptName VARCHAR(225),
	Note VARCHAR(MAX)
)
GO

ALTER TABLE dbo.Employee 
ADD CONSTRAINT CK_Level CHECK(Level BETWEEN 1 AND 7)

ALTER TABLE dbo.Employee
ADD CONSTRAINT CK_Status CHECK(status BETWEEN 0 AND 2)

ALTER TABLE dbo.Employee
ADD Email VARCHAR(225) UNIQUE

ALTER TABLE dbo.Employee
ADD CONSTRAINT CT_MgrNo DEFAULT (0) FOR MgrNo

ALTER TABLE dbo.Employee
ADD CONSTRAINT CT_MgrNo2 DEFAULT (0) FOR Status

ALTER TABLE dbo.Employee
ADD CONSTRAINT FK_DeptNo FOREIGN KEY(DeptNo)REFERENCES dbo.Department(DeptNo) 

GO

CREATE TABLE dbo.Skill
(
	SkillNo INT IDENTITY PRIMARY KEY,
	SkillName VARCHAR(225),
	Note VARCHAR(MAX)
)

GO

CREATE TABLE dbo.EmpSkill
(
	SkillNo INT NOT NULL,
	EmpNo INT NOT NULL,
	SkillLevel INT,
	RegDate DATE,
	Description VARCHAR(MAX)
)
GO

ALTER TABLE dbo.EmpSkill
ADD CONSTRAINT CK_SkillLevel CHECK(SkillLevel BETWEEN 1 AND 3)

ALTER TABLE dbo.EmpSkill
ADD CONSTRAINT PK_EmpSkill PRIMARY KEY (SkillNo, EmpNo)

ALTER TABLE dbo.EmpSkill 
ADD CONSTRAINT FK_EmpSkill FOREIGN KEY(SkillNo)REFERENCES dbo.Skill(SkillNo)

ALTER TABLE dbo.EmpSkill 
ADD CONSTRAINT FK_EmpSkill2 FOREIGN KEY(EmpNo)REFERENCES dbo.Employee(EmpNo)

ALTER TABLE dbo.EmpSkill
DROP COLUMN Description 

GO

----> CHECK INSERT, CONFLICT FK-----
INSERT INTO dbo.Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Level, Status, Note, Email)
VALUES (1, 'Nam', '1000-02-01', 1, 2, '1000-01-01', 100, 1, 0, 'noted', 'NamNT@fsoft.com.vn.vn')

INSERT INTO dbo.Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Level, Status, Note, Email)
VALUES (2, 'Mai', '1000-02-01', 2, 3, '1000-01-01', 50, 1, 1, 'noted', 'Maint@fsoft.com.vn') 

INSERT INTO dbo.Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Level, Status, Note, Email)
VALUES (3, 'Quang', '1996-03-05', 2, 1, '2000-01-02', 10, 2, 'noted', 'QuangNH@fsoft.com.vn')


INSERT INTO dbo.Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Level, Status, Note, Email)
VALUES (4, 'M', '1996-01-03', 4, 2, '2000-05-01', 12.9, 2, 2, 'noted', 'Ma@fsoft.com.vn')

INSERT INTO dbo.Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Level, Status, Note, Email)
VALUES (5, 'ai', '1996-08-01', 1, 1, '2000-07-01', 16.3, 2, 0, 'noted', 'aint@fsoft.com.vn')

GO

CREATE VIEW EMPLOYEE_TRACKING
AS
SELECT EmpNo, Emp_Name, Level FROM dbo.Employee
WHERE Level >=3 AND Level <= 5
GO

SELECT * FROM EMPLOYEE_TRACKING
GO

DROP DATABASE TrainingManagement
GO