USE master
IF NOT EXISTS (SELECT * FROM sys.databases WHERE NAME = 'Movie_Management')
CREATE DATABASE Movie_Management

GO 
----To avoid errors when running again, comment above script and use this script:
/*USE master
IF EXISTS (SELECT * FROM sys.databases WHERE NAME = 'Movie_Management')
DROP DATABASE Movie_Management
GO 

CREATE DATABASE Movie_Management

GO  */

USE Movie_Management
GO
CREATE TABLE dbo.Movie
(
	movie_id		INT IDENTITY(1,1) PRIMARY KEY,
	movie_name		NVARCHAR(225) NOT NULL,
	duration		INT NOT NULL, ---compute by minutes 
	genre			INT,
	director		NVARCHAR(225),
	amount_of_money DECIMAL(11,2),
	comments		NVARCHAR(MAX) 
)
GO

ALTER TABLE dbo.Movie
ADD CONSTRAINT CHK_GENRE CHECK(genre IN (1,2,3,4,5,6,7,8))
GO

ALTER TABLE dbo.Movie
ADD CONSTRAINT CHK_DURATION CHECK(duration >= 60) 
GO

ALTER TABLE dbo.Movie
ADD image_link VARCHAR(225) UNIQUE
GO

CREATE TABLE dbo.Actor
(
	actor_id				INT IDENTITY (1,1) PRIMARY KEY,
	actor_name				NVARCHAR(225),
	age						SMALLINT,
	average_movie_salary	DECIMAL(11,2),
	nationality				NVARCHAR(225)
)
GO

CREATE TABLE dbo.ActedIn
(
	movie_id INT NOT NULL,
	actor_id INT NOT NULL,
	PRIMARY KEY(movie_id, actor_id)
)
GO

ALTER TABLE dbo.ActedIn
ADD CONSTRAINT FK_ACTED_MOVIE FOREIGN KEY (movie_id) REFERENCES dbo.Movie (movie_id)
GO

ALTER TABLE dbo.ActedIn
ADD CONSTRAINT FK_ACTED_ACTOR FOREIGN KEY (actor_id) REFERENCES dbo.Actor(actor_id)
GO

SELECT * FROM dbo.Movie

INSERT INTO dbo.Movie 
(
	movie_name, 
	duration, 
	genre, 
	director, 
	amount_of_money, 
	comments,
	image_link
) 
VALUES ('Ky su song mekong', 175, 3, 'Nguyen Van An', 17000000, 'phim dat giai canh dieu vang','AAAAA')
GO

INSERT INTO dbo.Movie 
(
	movie_name, 
	duration, 
	genre, 
	director, 
	amount_of_money, 
	comments,
	image_link
) 
VALUES ('HeadShot', 300, 1, 'Hoang Khanh Ngoc', 9000000, 'phim dat giai canh dieu bac','BBB')
GO

INSERT INTO dbo.Movie 
(
	movie_name, 
	duration, 
	genre, 
	director, 
	amount_of_money, 
	comments,
	image_link
) 
VALUES ('Chang Vang', 65, 7, 'Mai Ngoc Anh', 6000000, 'phim du thi', 'CCC')
GO

INSERT INTO dbo.Movie 
(
	movie_name, 
	duration, 
	genre, 
	director, 
	amount_of_money, 
	comments,
	image_link
) 
VALUES ('Hung Nhai', 115, 4, 'Mark', 900000, 'phim du thi giai canh dieu bac', 'DDD')
GO

INSERT INTO dbo.Movie 
(
	movie_name, 
	duration, 
	genre, 
	director, 
	amount_of_money, 
	comments,
	image_link
) 
VALUES ('Ky su Hai Ha', 475, 8, 'Bentley', 20000000, 'phim dat giai canh dieu vang', 'EEEE')
GO

SELECT * FROM dbo.Actor

INSERT INTO dbo.Actor
(
	actor_name,
	age,
	average_movie_salary,
	nationality
)
VALUES ( 'Hoang Nam', 23, 2000000, 'Viet Nam')
GO

INSERT INTO dbo.Actor
(
	actor_name,
	age,
	average_movie_salary,
	nationality
)
VALUES ( 'Nha Phuong', 21, 3000000, 'Viet Nam')
GO

INSERT INTO dbo.Actor
(
	actor_name,
	age,
	average_movie_salary,
	nationality
)
VALUES ( 'Miu Le', 28, 5000000, 'Viet Nam')
GO

INSERT INTO dbo.Actor
(
	actor_name,
	age,
	average_movie_salary,
	nationality
)
VALUES ( 'Pham Hong Phuoc', 57, 4000000, 'Viet Nam')
GO

INSERT INTO dbo.Actor
(
	actor_name,
	age,
	average_movie_salary,
	nationality
)
VALUES ( 'Mark', 35, 6000000, 'My')
GO

UPDATE dbo.Actor
SET actor_name = 'Jessica'
WHERE actor_name = 'Mark' 
GO

SELECT * FROM dbo.ActedIn

INSERT INTO dbo.ActedIn
(
	movie_id,
	actor_id 
)
VALUES (1, 1)
GO

INSERT INTO dbo.ActedIn
(
	movie_id,
	actor_id 
)
VALUES (1, 2)
GO

INSERT INTO dbo.ActedIn
(
	movie_id,
	actor_id 
)
VALUES (2, 1)
GO

INSERT INTO dbo.ActedIn
(
	movie_id,
	actor_id 
)
VALUES (3, 5)
GO

INSERT INTO dbo.ActedIn
(
	movie_id,
	actor_id 
)
VALUES (4, 3)
GO

SELECT * FROM dbo.Actor WHERE age > 50 

SELECT * FROM dbo.Actor ORDER BY average_movie_salary
GO

SELECT * FROM dbo.ActedIn ai INNER JOIN dbo.Actor a ON ai.actor_id = a.actor_id
							 INNER JOIN dbo.Movie m ON ai.movie_id = m.movie_id
GO
SELECT * FROM dbo.Movie WHERE movie_id IN(SELECT movie_id FROM dbo.ActedIn 
GROUP BY movie_id
HAVING COUNT(actor_id) > 3) 
AND genre = 1
GO
	




