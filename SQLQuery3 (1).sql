USE master
IF EXISTS(SELECT * FROM sys.databases WHERE name = 'Trainee_Table')
DROP DATABASE Trainee_Table

CREATE DATABASE Trainee_Table
GO

USE Trainee_Table
CREATE TABLE dbo.TraineeInformation
(TraineeID INT IDENTITY (1,1) PRIMARY KEY,
FullName NVARCHAR (225) NOT NULL,
BirthDate DATE,
Gender BIT,
ET_IQ INT,
ET_Gmath INT,
ET_English INT,
TrainingClass VARCHAR (20),
EvaluationNotes NVARCHAR(MAX),
CONSTRAINT CHK_TraineeInformation CHECK (ET_IQ>0 AND ET_IQ<20 AND ET_Gmath>0 AND ET_Gmath<20 AND ET_English>0 AND ET_English<50))

INSERT INTO dbo.TraineeInformation(TraineeID, FullName, BirthDate, Gender, ET_IQ, ET_Gmath, ET_English, TrainingClass, EvaluationNotes)VALUES (1, 'Yen Trinh', '1000-02-03', 1, 12, 12, 45, 'A1', 'Danh gia dua tren co so')

INSERT INTO dbo.TraineeInformation(TraineeID, FullName, BirthDate, Gender, ET_IQ, ET_Gmath, ET_English, TrainingClass, EvaluationNotes)VALUES (2, 'Johns', '1000-02-03', 0, 11, 10, 11, 'A2', 'Code value')
INSERT INTO dbo.TraineeInformation(TraineeID, FullName, BirthDate, Gender, ET_IQ, ET_Gmath, 
ET_English, TrainingClass, EvaluationNotes)VALUES (3, 'Mark', '1000-04-05', 15, 17, 9, 22, 'A2', NULL)
INSERT INTO dbo.TraineeInformation(TraineeID, FullName, BirthDate, Gender, ET_IQ, ET_Gmath, 
ET_English, TrainingClass, EvaluationNotes)VALUES (4, 'Tom', '1000-01-01', 11, 7, 1, 2,'A0', 'FAIL')
INSERT INTO dbo.TraineeInformation(TraineeID, FullName, BirthDate, Gender, ET_IQ, ET_Gmath, 
ET_English, TrainingClass, EvaluationNotes)VALUES (5, 'Clare', '2000-01-01', 1, 1, 1, 1, 'A0', 'FAIL')
INSERT INTO dbo.TraineeInformation(TraineeID, FullName, BirthDate, Gender, ET_IQ, ET_Gmath, 
ET_English, TrainingClass, EvaluationNotes)VALUES (6, 'Tom', '1000-09-09', 10, 10, 10, 10, 'A4', NULL)
INSERT INTO dbo.TraineeInformation(TraineeID, FullName, BirthDate, Gender, ET_IQ, ET_Gmath, 
ET_English, TrainingClass, EvaluationNotes)VALUES (7, 'MCKain', '1000-01-02', 20, 20, 20, 20, 'A5', NULL)
INSERT INTO dbo.TraineeInformation(TraineeID, FullName, BirthDate, Gender, ET_IQ, ET_Gmath, 
ET_English, TrainingClass, EvaluationNotes)VALUES (8, 'Ha', '1000-10-10', 10, 10, 10, 10, 'A2', 'OK')
INSERT INTO dbo.TraineeInformation(TraineeID, FullName, BirthDate, Gender, ET_IQ, ET_Gmath, 
ET_English, TrainingClass, EvaluationNotes)VALUES (9, 'Lien', '1000-03-04', 19, 19, 19, 19, 'A1', 'Danh gia dua tren co so')
INSERT INTO dbo.TraineeInformation(TraineeID, FullName, BirthDate, Gender, ET_IQ, ET_Gmath, 
ET_English, TrainingClass, EvaluationNotes)VALUES (10, 'Lan', '1000-03-07', 17, 17, 17, 17, 'A1', NULL)

GO


