create database ZOO
go
use ZOO
go

create table animal_Type(
	animal_Type varchar(50) primary key
	)
go
create table case_Zoo(
	case_Id int identity(1,1) primary key,
	space varchar(50) check(space in ('yes', 'no')),
	height numeric(3,2)
	)
go
create table disease(
	disease_Name varchar(100) primary key,
	beginning_time datetime,
	duration int --day
    )
create table animal(
	animal_Id int identity(1,1) primary key,
	gender varchar(50), check (gender in ('female','male')),
	age int,
	animalType varchar(50) references animal_Type(animal_Type),
	case_Id int references case_Zoo(case_Id)
	)
go
create table type_Keeper(
	employee_Id int identity(1,1) primary key,
	name varchar(150),
	SSN int,
	andress varchar(150),
	phone numeric(11,0),
	animal_Type varchar(50) references animal_Type(animal_Type)
	)
go
create table haveDisease(
	animal_Id int references animal(animal_Id),
	disease_Name varchar(100) references disease(disease_Name),
	primary key(animal_Id, disease_Name)
	)


