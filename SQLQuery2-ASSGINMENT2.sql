USE master
IF EXISTS (SELECT * FROM sys.databases WHERE name = 'TraineeInfomation')
DROP DATABASE TraineeInfomation

CREATE DATABASE TraineeInfomation
GO
USE TraineeInfomation
GO

CREATE TABLE dbo.Trainee
(
TraineeID INT IDENTITY(1,1) PRIMARY KEY,
Full_Name NVARCHAR (225),
Birth_Date DATE,
Gender BIT,
ET_IQ INT, 
ET_Gmath INT,
ET_English INT,
Training_Class nvarchar (225),
Evaluation_Notes VARCHAR(MAX),
)
GO

ALTER TABLE dbo.Trainee
ADD CONSTRAINT ck_Trainee CHECK(ET_IQ BETWEEN 0 AND 20 AND ET_Gmath BETWEEN 0 AND 20 AND ET_English BETWEEN 0 AND 50)

ALTER TABLE dbo.Trainee
ADD Fsoft_Account VARCHAR(50) NOT NULL UNIQUE
GO

INSERT INTO dbo.Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, 
ET_English, Training_Class, Evaluation_Notes, Fsoft_Account) VALUES ('Mark', '1000-01-01', 1, 12, 11, 10, 'Java1', 'noted', 'MarkTT1')

INSERT INTO dbo.Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, 
ET_English, Training_Class, Evaluation_Notes, Fsoft_Account) VALUES ('Ha', '1000-10-01', 0, 8, 10, 30, 'Java2', 'noted', 'HaNH2')

INSERT INTO dbo.Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, 
ET_English, Training_Class, Evaluation_Notes, Fsoft_Account) VALUES ('Minh', '1000-01-01', 0, 2, 15, 20, 'Java3', 'noted', 'MinhTT1')

INSERT INTO dbo.Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, 
ET_English, Training_Class, Evaluation_Notes, Fsoft_Account) VALUES ('Lan', '1000-01-01', 1, 19, 11, 40, 'Java2', 'noted', 'LanTT1')

INSERT INTO dbo.Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, 
ET_English, Training_Class, Evaluation_Notes, Fsoft_Account) VALUES ('Mai', '1000-01-01', 1, 18, 11, 18, 'Java1', 'noted', 'MaiH1')

INSERT INTO dbo.Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, 
ET_English, Training_Class, Evaluation_Notes, Fsoft_Account) VALUES ('Gaus', '1000-01-01', 0, 15, 11, 19, 'Java2', 'noted', 'GausH1')

INSERT INTO dbo.Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, 
ET_English, Training_Class, Evaluation_Notes, Fsoft_Account) VALUES ('Tien', '1000-01-01', 0, 12, 10, 37, 'Java3', 'noted', 'TienNH')

INSERT INTO dbo.Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, 
ET_English, Training_Class, Evaluation_Notes, Fsoft_Account) VALUES ('Khanh', '1000-01-01', 1, 11, 12, 18, 'Java2', 'noted', 'KhanhNH')

INSERT INTO dbo.Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, 
ET_English, Training_Class, Evaluation_Notes, Fsoft_Account) VALUES ('Duc', '1000-01-01', 1, 8, 11, 12, 'Java1', 'noted', 'DucTT1')

INSERT INTO dbo.Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, 
ET_English, Training_Class, Evaluation_Notes, Fsoft_Account) VALUES ('Mark', '1000-01-01', 1, 13, 11, 10, 'Java2', 'noted', 'MarkTT2')

CREATE VIEW PassedTrainee 
AS
SELECT Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes, Fsoft_Account 
FROM dbo.Trainee
WHERE ET_IQ + ET_Gmath >=20 OR ET_IQ>=8 OR ET_Gmath>=8 OR ET_English>=18
GO

SELECT	TraineeID,
	Full_Name,
	Birth_Date
FROM	Trainee
WHERE	ET_IQ + ET_Gmath >= 20
	AND ET_IQ >= 8
	AND ET_Gmath >= 8
	AND ET_English >= 18
ORDER BY MONTH(Birth_Date)

SELECT	TraineeID,
	Full_Name,
	Birth_Date,
	YEAR(GETDATE()) - YEAR(Birth_Date) AS AGE,
	Gender
FROM	Trainee
WHERE	LEN(Full_Name) = (SELECT MAX(LEN(Full_Name)) FROM Trainee)

DROP DATABASE Fresher_Training_Mngt
GO






