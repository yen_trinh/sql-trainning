USE master,
CREATE TABLE dbo.Departments (
Id INT NOT NULL AUTO_INCREMENT,
NAME NVARCHAR(50) NOT NULL,
PRIMARY KEY(Id),
);
INSERT INTO dbo.Departments
([Id], [Name])
VALUES
(1, 'HR'),
(2, 'Sale'),
(3, 'Tech')
;

CREATE TABLE dbo.Employees(
Id INT NOT NULL AUTO_INCREMENT,
FName NVARCHAR(35) NOT NULL,
LName NVARCHAR(35) NOT NULL,
PhoneNumber VARCHAR(11),
ManagerId INT,
Department INT NOT NULL,
Salary INT NOT NULL,
HireDate DATETIME NOT NULL,
PRIMARY KEY(Id),
FOREIGN KEY (ManagerId) REFERENCES dbo.Employees(Id),
FOREIGN KEY (DepartmentId) REFERENCES dbo.Departments(Id),
);

INSERT INTO dbo.Employees
([Id], [FName], [LName], [PhoneNumber], [ManagerId], [DepartmentId], [Salary], [HireDate])
VALUES
(1, 'James', 'Smith', 1234567890, NULL, 1, 1000, '01-01-2002')
(2, 'John', 'Johnson', 2468101214, '1', 1, 400, '23-03-2005')
(3, 'Michael', 'Williams', 1357911131, '1', 600, '12-05-2009')
(4, 'Johnathon', 'Smith', 1212121212, '2', 1, 500, '24-07-2016')
;

CREATE TABLE dbo.Customers(
Id INT NOT NULL AUTO_INCREMENT
FName NVARCHAR (35) NOT NULL,
LName NVARCHAR (35) NOT NULL,
Email VARCHAR (100) NOT NULL,
PhoneNumber VARCHAR (11),
PreferredContact NVARCHAR (6) NOT NULL,
PRIMARY KEY(Id),
);

INSERT INTO dbo.Customers
([Id], [FName], [LName], [Email], [PhoneNumber], [PreferredContact])
VALUES
(1, 'william', 'Johnes', 'william.john@example.com', '3347927472', 'PHONE')
(2, 'david', 'Miller', 'dmiller@example.net', '31237747900', 'EMAIL')
(3, 'Richard', 'Davis', 'richard0123@example.com', NULL, 'EMAIL')
;
