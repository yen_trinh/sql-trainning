USE master
IF NOT EXISTS (SELECT * FROM sys.databases WHERE NAME = 'EmpMgnt')
CREATE DATABASE EmpMgnt

GO 
----To avoid errors when running again, comment above script and use this script:
/*USE master
IF EXISTS (SELECT * FROM sys.databases WHERE NAME = 'EmpMgnt')
DROP DATABASE EmpMgnt
GO 

CREATE DATABASE EmpMgnt

GO  */

USE EmpMgnt
GO
CREATE SCHEMA employee

CREATE TABLE employee.Department
(
	dept_no		INT IDENTITY PRIMARY KEY,
	dept_name   NVARCHAR(50),
	note		NVARCHAR(MAX)
)
GO

CREATE TABLE employee.Employee
(
	emp_no		 INT IDENTITY PRIMARY KEY,
	emp_name	 NVARCHAR(50),
	birth_day	 DATE,
	email		 VARCHAR(50),
	dept_no		 INT,
	mgr_no		 INT NOT NULL,
	[start_date] DATE,
	salary		 DECIMAL(11,2),
	[level]		 INT,
	[status]     INT,
	note		 NVARCHAR(MAX)
)
GO

ALTER TABLE employee.Employee
ADD CONSTRAINT CHK_Level CHECK ([level] BETWEEN 1 AND 7)
GO

ALTER TABLE employee.Employee
ADD CONSTRAINT status_def DEFAULT 0 FOR [status]
GO

ALTER TABLE employee.Employee
ADD CONSTRAINT mgr_def DEFAULT 0 FOR mgr_no
GO

ALTER TABLE employee.Employee
ADD CONSTRAINT CHK_Status CHECK ([status] BETWEEN 0 AND 2)
GO

CREATE TABLE employee.Skill
(
	skill_no	INT IDENTITY PRIMARY KEY,
	skill_name	NVARCHAR(100),
	note		NVARCHAR(MAX)
)
GO

CREATE TABLE employee.Emp_skill
(
	skill_no INT NOT NULL,
	emp_no INT NOT NULL,
	skill_level INT,
	reg_date DATE,
	[description] NVARCHAR(MAX) 
)
GO

ALTER TABLE employee.Emp_skill
ADD CONSTRAINT CHK_SLevel CHECK (skill_level BETWEEN 1 AND 3)
GO

ALTER TABLE employee.Emp_skill
ADD CONSTRAINT PK_Empskil PRIMARY KEY (skill_no, emp_no)
GO

ALTER TABLE employee.Emp_skill
ADD CONSTRAINT FK_Skillno1 FOREIGN KEY (emp_no) REFERENCES employee.Employee(emp_no)
GO

ALTER TABLE employee.Emp_skill
ADD CONSTRAINT FK_Skillno FOREIGN KEY (skill_no) REFERENCES employee.Skill(skill_no)
GO

SELECT * FROM employee.Department
GO

INSERT INTO employee.Department 
(
	dept_name,
	note
)
VALUES ('FA', 'fresher academy')
GO

INSERT INTO employee.Department 
(
	dept_name,
	note
)
VALUES ('Fsu1', 'aaa')
GO

INSERT INTO employee.Department 
(
	dept_name,
	note
)
VALUES ('Fsu2', 'bbb')
GO

INSERT INTO employee.Department 
(
	dept_name,
	note
)
VALUES ('Fsu3', 'ccc')
GO

INSERT INTO employee.Department
(
	dept_name,
	note
)
VALUES ('Fsu4', 'ddd')
GO

INSERT INTO employee.Department 
(
	dept_name,
	note
)
VALUES ('Fsu5', 'eee')
GO

INSERT INTO employee.Department
(
	dept_name,
	note
)
VALUES ('Fsu6', 'fff')
GO

INSERT INTO employee.Department 
(
	dept_name,
	note
)
VALUES ('Fsu7', 'ggg')
GO

SELECT * FROM employee.Employee

INSERT INTO employee.Employee
(
	emp_name,
	birth_day,
	email,
	dept_no,
	mgr_no,
	[start_date],
	salary,
	[level],
	[status],
	note
)
VALUES ('hoang', '1996-01-23', 'hoangcho@gmail.com', 1, 1, '2017-12-12', 125, 1, 0, 'aaa')
GO

INSERT INTO employee.Employee
(
	emp_name,
	birth_day,
	email,
	dept_no,
	mgr_no,
	[start_date],
	salary,
	[level],
	[status],
	note
)
VALUES ('hoa', '1992-01-01', 'hoahoang@gmail.com', 1, 2, '2016-01-01', 300, 2, 1, 'bbb')
GO

INSERT INTO employee.Employee
(
	emp_name,
	birth_day,
	email,
	dept_no,
	mgr_no,
	[start_date],
	salary,
	[level],
	[status],
	note
)
VALUES ('mai', '1988-01-01', 'mai@gmail.com', 2, 3, '2015-03-03', 200, 3, 2, 'ccc')
GO

INSERT INTO employee.Employee
(
	emp_name,
	birth_day,
	email,
	dept_no,
	mgr_no,
	[start_date],
	salary,
	[level],
	[status],
	note
)
VALUES ('hoa', '1992-01-05', 'hoa2hoang@gmail.com', 2, 3, '2016-01-01', 200, 1, 1, 'ddd')
GO

INSERT INTO employee.Employee
(
	emp_name,
	birth_day,
	email,
	dept_no,
	mgr_no,
	[start_date],
	salary,
	[level],
	[status],
	note
)
VALUES ('hoa anh', '1992-11-01', 'hoaanh@gmail.com', 3, 2, '2016-01-05', 300, 2, 1, 'eee')
GO

INSERT INTO employee.Employee
(
	emp_name,
	birth_day,
	email,
	dept_no,
	mgr_no,
	[start_date],
	salary,
	[level],
	[status],
	note
)
VALUES ('ngoc anh', '1997-01-11', 'anhhoang@gmail.com', 3, 1, '2016-01-01', 100, 1, 0, 'fff')
GO

INSERT INTO employee.Employee
(
	emp_name,
	birth_day,
	email,
	dept_no,
	mgr_no,
	[start_date],
	salary,
	[level],
	[status],
	note
)
VALUES ('lan', '1992-01-01', 'anhlan@gmail.com', 3, 1, '2018-01-01', 300, 2, 1, 'ggg')
GO

INSERT INTO employee.Employee
(
	emp_name,
	birth_day,
	email,
	dept_no,
	mgr_no,
	[start_date],
	salary,
	[level],
	[status],
	note
)
VALUES ('tuan', '1992-11-01', 'tuanhoang@gmail.com', 3, 1, '2016-11-01', 800, 3, 1, 'hhh')
GO

INSERT INTO employee.Skill
(
	skill_name,
	note
)
VALUES ('Java', 'jjjj')
GO

INSERT INTO employee.Skill
(
	skill_name,
	note
)
VALUES ('C#', 'aaa')
GO

INSERT INTO employee.Skill
(
	skill_name,
	note
)
VALUES ('C++', 'bbb')
GO

INSERT INTO employee.Skill
(
	skill_name,
	note
)
VALUES ('C', 'ccc')
GO

INSERT INTO employee.Skill
(
	skill_name,
	note
)
VALUES ('.NET', 'ddd')
GO

INSERT INTO employee.Skill
(
	skill_name,
	note
)
VALUES ('HTML', 'eee')
GO

INSERT INTO employee.Skill
(
	skill_name,
	note
)
VALUES ('Hibernate', 'fff')
GO

INSERT INTO employee.Skill
(
	skill_name,
	note
)
VALUES ('Spring', 'ggg')
GO

INSERT INTO employee.Emp_skill
(
	skill_no,
	emp_no,
	skill_level,
	reg_date,
	[description] 
)
VALUES (1, 1, 3, '2018-12-12', 'aaaa')
GO

INSERT INTO employee.Emp_skill
(
	skill_no,
	emp_no,
	skill_level,
	reg_date,
	[description] 
)
VALUES (2, 2, 1, '2018-12-31', 'bbbb')
GO

INSERT INTO employee.Emp_skill
(
	skill_no,
	emp_no,
	skill_level,
	reg_date,
	[description] 
)
VALUES (3, 3, 3, '2017-02-01', 'CCC')
GO

INSERT INTO employee.Emp_skill
(
	skill_no,
	emp_no,
	skill_level,
	reg_date,
	[description] 
)
VALUES (4, 1, 2, '2015-03-03', 'DDD')
GO

INSERT INTO employee.Emp_skill
(
	skill_no,
	emp_no,
	skill_level,
	reg_date,
	[description] 
)
VALUES (5, 5, 3, '2017-03-03', 'EEE')
GO

INSERT INTO employee.Emp_skill
(
	skill_no,
	emp_no,
	skill_level,
	reg_date,
	[description] 
)
VALUES (6, 6, 2, '2018-01-01', 'FFFF')
GO

INSERT INTO employee.Emp_skill
(
	skill_no,
	emp_no,
	skill_level,
	reg_date,
	[description] 
)
VALUES (7, 7, 1, '2018-05-05', 'GGGG')
GO

INSERT INTO employee.Emp_skill
(
	skill_no,
	emp_no,
	skill_level,
	reg_date,
	[description] 
)
VALUES (8, 8, 3, '2018-09-09', 'HHHH')
GO
---------------- question 2
SELECT e.emp_name, d.dept_name, e.email FROM employee.Employee e INNER JOIN employee.Department d
ON e.dept_no = d.dept_no
WHERE DATEDIFF(MONTH, [start_date], GETDATE()) >= 6
GO

----------------- question 3
SELECT * FROM employee.Employee e INNER JOIN employee.Emp_Skill es ON e.emp_no = es.emp_no
						INNER JOIN employee.Skill s ON es.skill_no = s.skill_no
						WHERE s.skill_name IN ('C++', '.NET')
GO
---------------question4
SELECT DISTINCT e.emp_name AS Employee, m.mgr_no AS reports_to, m.emp_name AS Manager, m.email AS manager_email
FROM employee.Employee e
INNER JOIN employee.Employee m ON e.mgr_no = m.emp_no;
GO

---------------question5
SELECT e.emp_name, d.dept_name FROM employee.Employee e INNER JOIN employee.Department d ON e.dept_no = d.dept_no
WHERE d.dept_no IN (SELECT emp.dept_no  FROM employee.Employee emp GROUP BY emp.dept_no HAVING COUNT(emp.emp_no) >= 2 )
GO

--------------question6
SELECT * FROM employee.Employee ORDER BY emp_name
GO

-------------question7
SELECT e.emp_name, e.birth_day, e.email 
FROM employee.Employee e INNER JOIN employee.Emp_Skill es ON e.emp_no = es.emp_no
WHERE e.[status] = 0 AND e.emp_no IN (SELECT emp_no FROM employee.Emp_Skill GROUP BY emp_no HAVING COUNT(skill_no) >=2)
GO
------------question8

CREATE VIEW emp_working ---ERROR: CREATE VIEW must be the only statement on the batch
AS
SELECT e.emp_name, d.dept_name, s.skill_name 
FROM employee.Employee e 
INNER JOIN employee.Department d ON e.dept_no = d.dept_no
INNER JOIN employee.Emp_Skill es ON es.emp_no = e.emp_no
INNER JOIN employee.Skill s ON es.skill_no = s.skill_no

SELECT * FROM emp_working 
GO
