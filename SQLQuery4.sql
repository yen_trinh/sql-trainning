﻿----> Question 1
USE master
IF EXISTS(SELECT * FROM sys.databases WHERE NAME = 'Employee_Table')
DROP DATABASE Employee_Table

CREATE DATABASE Employee_Table
GO

USE Employee_Table
CREATE TABLE dbo.EMPLOYEE
(
	EmpNo INT NOT NULL,
	EmpName NVARCHAR(225) NOT NULL,
	BirthDay DATE NOT NULL,
	DeptNo INT NOT NULL,
	MgrNo VARCHAR(25) NOT NULL,
	StartDate DATE NOT NULL,
	Salary MONEY, 
	EmpLevel INT NOT NULL, 
	EmpStatus INT NOT NULL, 
	Note TEXT
)
ALTER TABLE dbo.EMPLOYEE ADD CONSTRAINT CK_EMPLOYEE CHECK (EmpLevel BETWEEN 1 AND 7 AND EmpStatus BETWEEN 0 AND 2)
ALTER TABLE dbo.EMPLOYEE ADD CONSTRAINT PK_EMPLOYEE PRIMARY KEY(EmpNo)
---> Question 2
ALTER TABLE dbo.EMPLOYEE ADD Email VARCHAR(50) UNIQUE
ALTER TABLE dbo.EMPLOYEE ADD CONSTRAINT DEF1 DEFAULT 0 FOR MgrNo
ALTER TABLE dbo.EMPLOYEE ADD CONSTRAINT DEF2 DEFAULT 0 FOR EmpStatus
----> End of question 2


----> Question 1(continue)
CREATE TABLE dbo.SKILL
(
	SkillNo INT IDENTITY NOT NULL,
	SkillName NVARCHAR(225) NOT NULL,
	Note TEXT
)
ALTER TABLE dbo.SKILL ADD CONSTRAINT PK_SKILL PRIMARY KEY(SkillNo)
GO

CREATE TABLE dbo.EMP_SKILL
(
	SkillNo INT NOT NULL,
	EmpNo INT NOT NULL,
	SkillLevel INT NOT NULL,
	RegDate DATE,
	SkillDescription TEXT
)
ALTER TABLE dbo.EMP_SKILL ADD CONSTRAINT PK_EMP_SKILL PRIMARY KEY(SkillNo, EmpNo)
ALTER TABLE dbo.EMP_SKILL ADD CONSTRAINT FK_EMP_SKILL FOREIGN KEY(SkillNo) REFERENCES dbo.SKILL(SkillNo)
ALTER TABLE dbo.EMP_SKILL ADD CONSTRAINT FK_EMP_SKILL2 FOREIGN KEY(EmpNo) REFERENCES dbo.EMPLOYEE(EmpNo)
ALTER TABLE dbo.EMP_SKILL ADD CONSTRAINT CK_EMP_SKILL CHECK (SkillLevel BETWEEN 1 AND 3) 
GO


CREATE TABLE dbo.DEPARTMENT
(
DeptNo INT IDENTITY NOT NULL,
DeptName NVARCHAR(225) NOT NULL,
Note TEXT
)
ALTER TABLE dbo.DEPARTMENT ADD CONSTRAINT PK_DEPARTMENT PRIMARY KEY(DeptNo)

---> Question 3
ALTER TABLE dbo.EMPLOYEE ADD CONSTRAINT FK_EMPLOYEE FOREIGN KEY (DeptNo) REFERENCES dbo.DEPARTMENT(DeptNo)

ALTER TABLE dbo.EMP_SKILL DROP COLUMN SkillDescription 

--> Question 4
SELECT * FROM dbo.SKILL
INSERT INTO dbo.SKILL(SkillName) VALUES ('Ke toan')
INSERT INTO dbo.SKILL(SkillName) VALUES ('Quan tri');
INSERT INTO dbo.SKILL(SkillName) VALUES ('Dao tao');
INSERT INTO dbo.SKILL(SkillName) VALUES ('XNK');
INSERT INTO dbo.SKILL(SkillName) VALUES ('Nhan su');

SELECT * FROM dbo.DEPARTMENT
INSERT INTO dbo.DEPARTMENT(DeptName) VALUES ('Accounting');
INSERT INTO dbo.DEPARTMENT(DeptName) VALUES ('FHO');
INSERT INTO dbo.DEPARTMENT(DeptName) VALUES ('CTC');
INSERT INTO dbo.DEPARTMENT(DeptName) VALUES ('PRC');
INSERT INTO dbo.DEPARTMENT(DeptName) VALUES ('HR');

SELECT * FROM dbo.EMPLOYEE
INSERT INTO EMPLOYEE(EmpNo,EmpName, BirthDay, DeptNo, MgrNo, StartDate, EmpLevel, EmpStatus)
VALUES (1, 'A', '19960403', 1, 1, '20180401', 1, 0)
INSERT INTO EMPLOYEE(EmpNo,EmpName, BirthDay, DeptNo, MgrNo, StartDate, EmpLevel, EmpStatus, Email)
VALUES (2, 'B', '19951007', 3, 3, '20180101', 3, 0, 'b@fsoft.com.vn')
INSERT INTO EMPLOYEE(EmpNo,EmpName, BirthDay, DeptNo, MgrNo, StartDate, EmpLevel, EmpStatus, Email)
VALUES (3, 'C', '19951103', 2, 1, '20180101', 1, 2, 'c@fsoft.com.vn')
INSERT INTO EMPLOYEE(EmpNo,EmpName, BirthDay, DeptNo, MgrNo, StartDate, EmpLevel, EmpStatus, Email)
VALUES (4, 'D', '19960219', 4, 2, '20170401', 2, 0, 'd@fsoft.com.vn')
INSERT INTO EMPLOYEE(EmpNo,EmpName, BirthDay, DeptNo, MgrNo, StartDate, EmpLevel, EmpStatus, Email)
VALUES (5, 'E', '19950403', 3, 3, '20180401', 5, 1, 'e@fsoft.com.vn')

SELECT * FROM EMP_SKILL
INSERT INTO EMP_SKILL(SkillNo, EmpNo, SkillLevel) VALUES (1, 1, 1);
INSERT INTO EMP_SKILL(SkillNo, EmpNo, SkillLevel) VALUES (1, 2, 3);
INSERT INTO EMP_SKILL(SkillNo, EmpNo, SkillLevel) VALUES (2, 1, 2);
INSERT INTO EMP_SKILL(SkillNo, EmpNo, SkillLevel) VALUES (2, 2, 3);
INSERT INTO EMP_SKILL(SkillNo, EmpNo, SkillLevel) VALUES (3, 1, 1);
GO
	
CREATE VIEW EMPLOYEE_TRACKING AS
SELECT EmpNo,EmpName,EmpLevel FROM dbo.EMPLOYEE
WHERE EmpLevel BETWEEN 3 AND 5
GO

SELECT * FROM EMPLOYEE_TRACKING 








 



